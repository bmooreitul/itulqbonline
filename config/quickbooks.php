<?php

use App\Models\User;

return [
	'sass' 					=> true, 			//TOGGLE BETWEEN A SINGLE CONNECTION OR MULTIPLE CONNECTIONS
	'data_service' 			=> [
		'auth_mode'     	=> 'oauth2',
		'base_url'      	=> env('QUICKBOOKS_API_URL', config('app.env') === 'production' ? 'Production' : 'Development'),
		'client_id'     	=> env('QUICKBOOKS_CLIENT_ID'),
		'client_secret' 	=> env('QUICKBOOKS_CLIENT_SECRET'),
		'scope'         	=> 'com.intuit.quickbooks.accounting',
	],
	'logging' 				=> [
		'enabled' 			=> env('QUICKBOOKS_DEBUG', config('app.debug')),
		'location' 			=> storage_path('logs'),
	],
	'route' 				=> [
		'middleware'        => [               // Controls the middlewares for thr routes.  Can be a string or array of strings
			'authenticated' => 'auth',         // Added to the protected routes for the package (i.e. connect & disconnect)        
			'default'       => 'web',          // Added to all of the routes for the package
		],
		'paths'             => [            
			'connect'       => 'connect',      // Show forms to connect/disconnect   
			'disconnect'    => 'disconnect',   // The DELETE takes place to remove token          
			'token'         => 'token',        // Return URI that QuickBooks sends code to allow getting OAuth token
		],
		'prefix'     		=> 'quickbooks',
	],

	/*
	|--------------------------------------------------------------------------
	| Properties for control the "user" relationship in Token
	|--------------------------------------------------------------------------
	|
	| The Token class has a "user" relationship, and these properties allow
	| configuring the relationship.
	|
	*/

	'user' 				=> [
		'keys'  		=> [
			'foreign' 	=> 'user_id',
			'owner'   	=> 'id',
		],
		'model' 		=> User::class,
	],
];
