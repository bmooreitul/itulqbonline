<?php

namespace Itul\QuickBooks\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Itul\QuickBooks\Client;

/**
 * Class ClientServiceProvider
 *
 * @package Itul\QuickBooks
 */
class ClientServiceProvider extends LaravelServiceProvider{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(){
        return [
            Client::class,
        ];
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->app->bind(Client::class, function (Application $app) {
            if(is_null(config('quickbooks.sass')) || config('quickbooks.sass') === true){
                $token = ($app->auth->user()->quickBooksToken) ? : $app->auth->user()->quickBooksToken()->make();
            }
            else{
                $token = (\Itul\QuickBooks\Token::count() ? \Itul\QuickBooks\Token::first() : (new \Itul\QuickBooks\Token)->make()); 
            }
            
            return new Client($app->config->get('quickbooks'), $token);
        });

        $this->app->alias(Client::class, 'QuickBooks');
        $this->app->alias(\Itul\QuickBooks\Helpers\QuickBooks::class, 'QBHelper');
    }
}
