<?php

namespace Itul\QuickBooks\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Itul\QuickBooks\Http\Middleware\Filter;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class ServiceProvider
 *
 * @package Spinen\QuickBooks
 */
class ServiceProvider extends LaravelServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerMiddleware();
        $this->registerMacros();
        $this->registerPublishes();
        $this->registerRoutes();
        $this->registerViews();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->mergeConfigFrom(__DIR__ . '/../../config/quickbooks.php', 'quickbooks');
    }

    /**
     * Register the middleware
     *
     * If a route needs to have the QuickBooks client, then make sure that the user has linked their account.
     *
     */
    public function registerMiddleware(){
        $this->app->router->aliasMiddleware('quickbooks', Filter::class);
    }

    public function registerMacros(){

        Blueprint::macro('quickbooksFields', function () {
            $this->bigInteger('quickbooks_id')->nullable();
            $this->bigInteger('quickbooks_synctoken')->nullable();
            $this->string('quickbooks_realm')->nullable();
        });

        Blueprint::macro('dropQuickbooksFields', function () {
            $this->dropColumn('quickbooks_id');
            $this->dropColumn('quickbooks_synctoken');
            $this->dropColumn('quickbooks_realm');
        });
    }

    /**
     * There are several resources that get published
     *
     * Only worry about telling the application about them if running in the console.
     *
     */
    protected function registerPublishes()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

            $this->publishes([
                __DIR__ . '/../../config/quickbooks.php' => config_path('quickbooks.php'),
            ], 'itul-quickbooks');

            $this->publishes([
                __DIR__ . '/../../database/migrations' => database_path('migrations'),
            ], 'itul-quickbooks');

            $this->publishes([
                __DIR__ . '/../../resources/views' => base_path('resources/views/vendor/quickbooks'),
            ], 'itul-quickbooks');
        }
    }

    /**
     * Register the routes needed for the registration flow
     */
    protected function registerRoutes()
    {
        $config = $this->app->config->get('quickbooks.route');

        $this->app->router->prefix($config['prefix'])
			->as('quickbooks.')
			->middleware($config['middleware']['default'])
			->namespace('Itul\QuickBooks\Http\Controllers')
			->group(function(Router $router) use($config){
				$router->get($config['paths']['connect'], 'Controller@connect')->middleware($config['middleware']['authenticated'])->name('connect');
				$router->delete($config['paths']['disconnect'], 'Controller@disconnect')->middleware($config['middleware']['authenticated'])->name('disconnect');
				$router->get($config['paths']['token'], 'Controller@token')->name('token');
		});
    }

    /**
     * Register the views
     */
    protected function registerViews(){
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'quickbooks');
    }
}
