<?php

namespace Itul\QuickBooks;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\Larabuilder\Skeleton\SkeletonClass
 */
class QuickBooksFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'quickbooks';
    }
}
