<?php

namespace Itul\QuickBooks;

use Illuminate\Database\Eloquent\Model;

class Token extends Model{
    
    protected $table = 'quickbooks_tokens';
    protected $dates = [
        'access_token_expires_at',
        'refresh_token_expires_at',
    ];

    protected $fillable = [
        'access_token',
        'access_token_expires_at',
        'realm_id',
        'refresh_token',
        'refresh_token_expires_at',
        'user_id',
    ];

    public function getHasValidAccessTokenAttribute(){
        return $this->access_token_expires_at && \Carbon\Carbon::now()->lt($this->access_token_expires_at);
    }

    public function getHasValidRefreshTokenAttribute(){
        return $this->refresh_token_expires_at && \Carbon\Carbon::now()->lt($this->refresh_token_expires_at);
    }

    public function parseOauthToken(\QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2AccessToken $oauth_token){
        // TODO: Deal with exception
        $this->access_token             = $oauth_token->getAccessToken();
        $this->access_token_expires_at  = \Carbon\Carbon::parse($oauth_token->getAccessTokenExpiresAt());
        $this->realm_id                 = $oauth_token->getRealmID();
        $this->refresh_token            = $oauth_token->getRefreshToken();
        $this->refresh_token_expires_at = \Carbon\Carbon::parse($oauth_token->getRefreshTokenExpiresAt());
        return $this;
    }

    public function forceRefresh(){
        return app('Itul\QuickBooks\Client')->forceRefreshToken($this);
    }

    public function remove(){
        $user = $this->user;
        $this->delete();
        return $user->quickBooksToken()->make();
    }

    public function user(){
        $config = config('quickbooks.user');
        return $this->belongsTo($config['model'], $config['keys']['foreign'], $config['keys']['owner']);
    }
}
