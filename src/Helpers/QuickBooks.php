<?php

	namespace Itul\QuickBooks\Helpers;

	class QuickBooks {

		public static $_instance;
		public static $_dataService;
		public static $_token;

		public function __construct(){
			self::$_instance = $this;
			self::$_dataService = $this->_dataService();
		}

		public static function init(){		
			$calledClass = '\\'.get_called_class();
			return new $calledClass;
		}

		/*
		//GET THE QUICKBOOKS DATASERVICE
		public function _dataService(){
			return app('Itul\QuickBooks\Client')->getDataService();
		}
		*/

		public static function _getClient($token = null){
			$client = (new \Itul\QuickBooks\Client(
				config('quickbooks'), 
				($token ? $token : new \Itul\QuickBooks\Token)
			));

			if(self::$_token) $client->setToken(self::$_token);

			return $client;
		}

		//GET THE QUICKBOOKS DATASERVICE
		public function _dataService($token = null){
			try{
				if(!is_null($token)) throw new \Exception("Create New Quickbooks Data Service");
				return app('Itul\QuickBooks\Client')->getDataService();
			}
			catch(\Exception $e){
				return self::_getClient($token)->getDataService();
				if(self::$_token) $client->setToken($token);
				return $client->getDataService();				
			}			
		}

		public static function _setToken(\Itul\QuickBooks\Token $token){

			if(!isset(self::$_instance)) self::init();

			//IF THE TOKEN IS ALREADY SET AND MATCHES DONT CHANGE THE DATA SERVICE
			if(self::$_token && $token->id == self::$_token->id) return self::$_instance;
			self::$_token = $token;

			self::$_dataService = self::$_instance->_dataService($token);

			//SET THE TOKEN TO THE QUICKBOOKS WRAPPER
			//app('Itul\QuickBooks\Client')->setToken($token);

			//UPDATE THE DATA SERVICE
			//self::$_dataService = self::$_instance->_dataService();

			//SEND BACK THE HELPER
			return self::$_instance;
		}

		public function downloadPDF($type, $id, $forceDownload = false){

			//TRY TO FIND THE RECORD
			if($item = $this->query()->from($type)->where('id', '=', $id)->get()->first()){

				//NAME A TEMP DIRECTORY
				$tmpDir = sys_get_temp_dir().'/'.md5(microtime(true).rand(0,999));

				//CREATE THE TEMP DIRECTORY
				mkdir($tmpDir, 0777, true);

				//DOWNLOAD THE PDF(s) TO THE TEMP DIRECTORY
				$res = $this->_dataService()->DownloadPDF($item, $tmpDir);

				//TRY TO SCAN THE TEMP DIRECTORY FOR PDFS
				if($files = glob($tmpDir.'/*.pdf')){

					//CHECK IF PDFS EXIST IN THE TEMP DIRECTORY
					if(is_array($files) && !empty($files)){

						//GET THE FILE PATH OF THE TEMP DOWNLOADED FILE
						$filePath = $files[0];

						if(!$forceDownload) return $filePath;

						//SET THE HEADERS FOR FORCED FILE DOWNLOAD
				        header("Pragma: public"); // required
				        header("Expires: 0");
				        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				        header("Cache-Control: private", false); // required for certain browsers
				        header("Content-Disposition: attachment; filename=\"".basename($filePath)."\";" );
				        header("Content-Transfer-Encoding: binary");
				        header("Content-Length: ". @filesize($filePath));
				        header( "Content-Description: File Transfer");
				        header("Content-Type: application/pdf");

				        //READ THE FILE TO THE BUFFER
				        @readfile($filePath);

				        //EXIT FROM THE REQUEST
				        exit;
					}
				}

				//NO PDFS WERE DOWNLOADED SO THROW AN ERROR
				throw new \Exception("Quickbooks didnt download the pdf correctly. Please try again.");
			}

			//NO ITEM WAS FOUND FROM THE QUERY SO THROW AN ERROR
			throw new \Exception("Unable to find the {$type} requested");

		}

		public function query($table = null){
			return new \Itul\QuickBooks\Helpers\QuickbooksQuery($table);
		}

		public function getCompanyInfo(){
			return $this->_dataService()->getCompanyInfo();
		}

		public function create($entity, array $data){

			//DEFINE DEFAULTS
			$success 		= false;
			$qbModelResult 	= null;

			try{

				//VALIDATE DATA
				if(empty($data)) throw new \Exception("Add data cannot be empty");

				//GET THE DATASERVICE
				$dataService 		= $this->_dataService();

				//PARSE THE ENTITY NAME FROM A MODEL IF NEEDED
				if(is_object($entity) && $entity instanceof \QuickBooksOnline\API\Data\IPPIntuitEntity){
					$entityNameParts 	= explode('\\', get_class($entity));
					$entityName 		= array_pop($entityNameParts);
					$entity 			= substr($entityName, 0, 3) == 'IPP' ? substr($entityName, 3) : $entityName;
				}

				//BUILD THE MODEL DATA
				$qbModelResource 	= \QuickBooksOnline\API\Facades\FacadeHelper::reflectArrayToObject($entity, $data);

				//CREATE THE MODEL
				$qbModelResult      = $dataService->Add($qbModelResource);

				//CHECK FOR ERRORS
				$error              = $dataService->getLastError();

				if($error){

					$qbModelMessage     = (object)[
						'code'          => $error->getHttpStatusCode(),
						'oauth_message' => $error->getOAuthHelperError(),
						'message'       => $error->getResponseBody()
					];
				}
				else{

					//SUCCESSFULL TRANSACTION
					$success            = true;

					//SET THE MESSAGE
					$qbModelMessage     = (object)[
						'code'          => 200,
						'oauth_message' => null,
						'message'       => 'Add request for '.$entity.' was successfull.'
					];
				}
			}
			catch(\Throwable $e){

				//SET THE MESSAGE
				$qbModelMessage     = (object)[
					'code'          => 500,
					'oauth_message' => null,
					'message'       => $e->getMessage()
				];
			}

			return (object)[
				'success' 	=> $success,
				'type' 		=> 'Add',
				'message' 	=> $qbModelMessage,
				'result' 	=> $qbModelResult,
				'model' 	=> null,
			];
			
		}

		public function update($entity, array $data){

			//DEFINE DEFAULTS
			$success 		= false;
			$qbModelResult 	= null;

			try{

				//INITAIL DATA VALIDATION
				if(!isset($data['Id'])) throw new \Exception("The Id is required to update");
				if(empty($data)) throw new \Exception("Update data cannot be empty");

				//GET THE DATASERVICE
				$dataService 		= $this->_dataService();

				//GET THE MODEL
				$qbModel            = (is_object($entity) && $entity instanceof \QuickBooksOnline\API\Data\IPPIntuitEntity) ? $entity : $dataService->FindbyId($entity, $data['Id']);

				//CHECK IF THE MODEL WAS LOADED
				if(!$qbModel) throw new \Exception("Could not find a record to update");

				//IF THE SYNCTOKEN WASNT PROVIDED ADD IT
				if(!array_key_exists('SyncToken', $data)) $data['SyncToken'] = $qbModel->SyncToken;

				//DEFAULT TO SPARSE UPDATE
				if(!array_key_exists('sparse', $data)) $data['sparse'] = true;

				//PARSE THE ENTITY NAME
				$entityNameParts 	= explode('\\', get_class($qbModel));
				$entityName 		= array_pop($entityNameParts);
				$entity 			= substr($entityName, 0, 3) == 'IPP' ? substr($entityName, 3) : $entityName;

				//MERGE THE ATTRIBUTES FOR THIS MODEL
				$newQbModel 		= \QuickBooksOnline\API\Facades\FacadeHelper::reflectArrayToObject($entity, $data);
		        $qbModelResource 	= \QuickBooksOnline\API\Facades\FacadeHelper::cloneObj($qbModel);
		        \QuickBooksOnline\API\Facades\FacadeHelper::mergeObj($qbModelResource, $newQbModel);

		        //UPDATE THE MODEL
				$qbModelResult      = $dataService->Update($qbModelResource);

				//CHECK FOR ERRORS
				$error                  = $dataService->getLastError();

				if($error){

					$qbModelMessage     = (object)[
						'code'          => $error->getHttpStatusCode(),
						'oauth_message' => $error->getOAuthHelperError(),
						'message'       => $error->getResponseBody()
					];
				}
				else{

					//SUCCESSFULL TRANSACTION
					$success            = true;

					//SET THE MESSAGE
					$qbModelMessage     = (object)[
						'code'          => 200,
						'oauth_message' => null,
						'message'       => 'Update request for '.$entity.' was successfull.'
					];
				}
			}
			catch(\Throwable $e){

				//SET THE MESSAGE
				$qbModelMessage     = (object)[
					'code'          => 500,
					'oauth_message' => null,
					'message'       => $e->getMessage()
				];
			}

			return (object)[
				'success' 	=> $success,
				'type' 		=> 'Update',
				'message' 	=> $qbModelMessage,
				'result' 	=> $qbModelResult,
				'model' 	=> null,
			];
		}
	}