<?php

	namespace Itul\QuickBooks\Helpers;
	
	class QuickbooksQuery extends \Itul\QuickBooks\Helpers\QuickBooks{

		private $_select 			= ['*'];
		private $_from;
		private $_where 			= [];
		private $_orderBy 			= [];
		private $_counting 			= false;
		private $_pagination 		= false;
		private $_startPosition 	= 0;
		private $_offset 			= 0;

		private $_paging = [
			'pageCount'		=> 0,
			'recordCount' 	=> 0,
			'currentPage' 	=> 1,
			'perPage' 		=> 20,
			'nextPage' 		=> 0,
			'previousPage' 	=> 0,	
		];

		public $success				= false;
		public $error 				= false;
		public $results;

		public function __construct($table = null){
			parent::__construct();
			$this->_from = $table;
			$this->results = collect([]);
			return $this;
		}

		public function __clone(){

			//RESET SOME STUFF WHEN CLONING
			$this->results 			= collect([]);
			$this->success 			= false;
			$this->error 			= false;
			$this->_startPosition 	= false;
		}

		public function all(){

			$res = collect([]);

			try{

				$page = $this->paginate(500)->get();				
				
				while(true){

					//BREAK OUT OF LOOP IF NOT A VALID PAGE
					if(!$page || !is_object($page) || !isset($page->success) || !$page->success || !isset($page->results) || !$page->results->count()) break;

					$res = $res->concat($page->results);

					$page = $page->nextPage();
				}
			}

			catch(\Exception $e){
				//SILENT
			}

			return $res;
		}

		public function find($id){

			//GET THE INSTANCE BY ID
			return \Itul\QuickBooks\Helpers\QuickBooks::$_dataService->FindbyId($this->_from, $id);
		}

		public function select(...$args){
			$this->_select = $args;
			return $this;
		}

		public function from($table){
			$this->_from = $table;
			return $this;
		}

		public function where(...$args){

			//DEFAULTS ALLOW FOR ONLY 2 ARGUMENTS AND ASSUMES = OPERATOR
			$field 		= $args[0];
			$operator 	= '=';
			$value 		= $args[1]; 

			//IF 3 ARGUMENTS WERE PASSED PARSE THEM
			if(count($args) === 3){
				$field 		= $args[0];
				$operator 	= strtoupper(trim($args[1]));
				$value 		= $args[2];
			}

			//START THE WHERE STRING
			$str =  $field.' '.$operator.' ';

			//IF THE OPERATOR IS AN IN
			if($operator == 'IN'){

				//IF THE VALUE IS AN ARRAY
				if(is_array($value)){

					//FORMAT THE ARRAY
					$formattedArr = ["("];
					foreach($value as $v) if(!empty(trim($v))) $formattedArr[] = "'".addslashes(trim($v))."'";					
					$str .= implode(',', $formattedArr).")";
				}

				//IF THE VALUE IS A STRING
				else{
					$str .= "'".addslashes($value)."'";
				}			
			}
			else{
				$str .= "'".addslashes($value)."'";
			}

			$this->_where[] = $str;

			return $this;
		}

		public function whereIn(string $field, array $value){
			return $this->where($field, 'IN', $value);
		}

		public function orderBy($field, $direction = 'ASC'){
			$this->_orderBy[] = $field.' '.$direction;
			return $this;
		}

		public function page($pos = 1){
			$this->_paging['currentPage'] = $pos;
			return $this;
		}

		public function offset($amount){
			$this->_offset = $amount;
			return $this;
		}

		public function limit($amount){
			$this->_paging['perPage'] = $amount;
			return $this;
		}

		public function first(){
			$oldPaging = $this->_paging;
			$this->_paging['perPage'] = 1;
			$res = $this->get()->first();
			$this->_paging = $oldPaging;
			return $res;
		}

		public function toQuery(){
			return $this->_buildQuery();
		}

		public function paginate(...$args){
			if(count($args) === 1){
				$this->limit($args[0]);
			}
			elseif(count($args) === 2){
				$this->limit($args[0]);
				$this->page($args[1]);
			}
			$this->_pagination = true;
			return $this->get();
		}

		public function count(){

			//ENABLE COUNTING
			$this->_counting = true;

			//GET THE RESULTS
			$res = $this->_getResults($this->_buildQuery(true));

			//DISABLE COUNTING
			$this->_counting = false;

			//RETURN THE RESULTS
			return $res-$this->_offset;
		}

		

		public function nextPage(){

			if($this->_pagination && $this->_paging['nextPage']){
				$clone = clone $this;
				return $clone->page($this->_paging['nextPage'])->get();
			}

			return null;
		}

		public function prevPage(){

			if($this->_pagination && $this->_paging['previousPage']){
				$clone = clone $this;
				return $clone->page($this->_paging['previousPage'])->get();
			}

			return null;
		}

		public function get($query = null){

			$this->results 		= $this->_getResults($query);
			$this->_requested 	= true;

			if(!$this->_pagination) return $this->results;

			return $this;
		}

		private function _getResults($query = null){

			//SET THE QUERY
			$query = is_null($query) ? $this->_buildQuery() : $query;
			
			//GET THE CONNECTION
			$dataService = \Itul\QuickBooks\Helpers\QuickBooks::$_dataService;

			//REQUEST THE QUERY
			$res = $dataService->query($query);

			//SET THE ERROR
			$this->error = $dataService->getLastError();

			//SET SUCCESS
			$this->success = !$this->error;

			//RETURN THE RESULT IF COUNTING
			if($this->_counting) return $res;

			//CALCULATE PAGING
			$this->_calcPaging();

			//RETURN A COLLECTION IF THERE WERE RESULTS
			if(is_array($res)) return collect($res);

			//RETURN AN EXMPTY COLLECTION
			return collect([]);
		}

		private function _calcPaging(){
			$this->_paging['recordCount'] 	= $this->count();
			$this->_paging['pageCount'] 	= ceil($this->_paging['recordCount']/$this->_paging['perPage']);
			$this->_paging['previousPage'] 	= $this->_paging['currentPage'] > 1 ? $this->_paging['currentPage']-1 : 0;
			$this->_paging['nextPage'] 		= $this->_paging['currentPage'] < $this->_paging['pageCount'] ? $this->_paging['currentPage']+1 : 0;
			return $this;
		}

		private function _buildQuery($count = false){			

			$q = [];
			$q[] = "SELECT";			
			$q[] = $count ? "COUNT(*)" : implode(',', $this->_select);
			$q[] = "FROM ".$this->_from;
			if(!empty($this->_where)) $q[] = "WHERE ".implode(" AND ", $this->_where);
			if(!empty($this->_orderBy)) $q[] = "ORDERBY ".implode(',', $this->orderBy);
			if(!$count){

				//SET THE START POSITION
				$this->_startPosition = $this->_paging['perPage']*($this->_paging['currentPage']-1)+1+$this->_offset;
				
				//ADD THE START POSITION TO THE QUERY
				if(!empty($this->_startPosition)) $q[] = "STARTPOSITION ".$this->_startPosition;

				//SET THE MAX RESULTS FOR THE QUERY
				if(!empty($this->_paging['perPage'])) $q[] = "MAXRESULTS ".$this->_paging['perPage'];
			}

			return implode(" ", $q);
		}
	}