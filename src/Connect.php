<?php

namespace Itul\QuickBooks;

trait Connect{	

	//SEND DATA TO QUICKBOOKS WITH OPTIONAL CALLBACK
	public function quickbooksSend($callback = null){
		return $this->_sendQuickbooksRequest((!isset($this->quickbooks_id) ? 'add' : 'update'), $callback);
	}

	public function quickbooksVoid($callback = null){
		return $this->_sendQuickbooksRequest('void', $callback);
	}

	public function quickbooksDelete($callback = null){
		return $this->_sendQuickbooksRequest('delete', $callback);
	}

	public function quickbooksRead($callback = null){
		return $this->_sendQuickbooksRequest('read', $callback);
	}

	public function quickbooksSoftCreate(){
		if(!isset($this->quickbooks_id)) $this->quickbooksSend();
		return $this;
	}

	public function quickbooksQuery(){
		
		//START THE QUERY
		$query = \Itul\QuickBooks\Helpers\QuickbooksQuery::init()->from($this->quickbooks_entity);

		//CHECK IF THIS MODEL EXISTS
		if($this->exists){

			//CHECK IF THIS MODEL HAS A QUICKBOOKS ID
			if(isset($this->quickbooks_id)) $query->where('Id', $this->quickbooks_id);

			//CHECK IF THIS MODEL HAS A FIELD TO SEARCH BY
			elseif(isset($this->quickbooks_findby) && is_array($this->quickbooks_findby)){

				//GET THE FIELD AND VALUE
				$findByField 	= array_keys($this->quickbooks_findby)[0];
				$findByValue 	= $this->{$this->quickbooks_findby[$findByField]};

				//FIND BY FIELD
				$query->where($findByField, 'LIKE', $findByValue);
			}
		}

		//SEND BACK THE QUERY
		return $query;		
	}

	public function quickbooksCanConnect(){
		return !is_null($this->_findQuickbooksAgencyToken());
	}

	//FORCE MODELS THAT USE THIS TRAIT TO HAVE THE GENERATE DATA METHOD
	abstract public function quickbooksGenerateData($type);

	private function _quickbooksFindByFields(){
		if(isset($this->quickbooks_findby) && is_array($this->quickbooks_findby)){
			
			//GET THE FIELD AND VALUE
			$findByField 	= array_keys($this->quickbooks_findby)[0];
			$findByValue 	= $this->{$this->quickbooks_findby[$findByField]};

			//QUERY FOR THE MODEL
			$res = \Itul\QuickBooks\Helpers\QuickBooks::init()
				->query()
				->from($this->quickbooks_entity)
				->where($findByField, 'LIKE', $findByValue)
				->limit(1)
				->get()->first();

			if($res && !isset($this->quickbooks_id)){
				$this->quickbooks_id 			= $res->Id;
				$this->quickbooks_synctoken 	= $res->SyncToken;

				//SET REALM VALUE FOR SASS IF NEEDED
				if(is_null(config('quickbooks.sass')) || config('quickbooks.sass') === true) if(in_array('quickbooks_realm', $this->getFillable())) if($token = $this->_findQuickbooksAgencyToken()) $this->quickbooks_realm = $token->realm_id;
				$this->saveQuietly();
			}

			return $res;
		}

		return null;
	}

	//SEND THE QUICKBOOKS REQUEST
	private function _sendQuickbooksRequest($type, $callback = null){

		//VERIFY THE ATTRIBUTES
		$this->_verifyQuickbooksAttributes();

		if(!$this->quickbooksCanConnect()) return;

		//DEFAULTS
		$success 				= false;
		$qbModelResult 			= null;

		//DEFAULT MESSAGE AS UN-AUTHENTICATED
		$qbModelMessage       	= (object)[
			'code'          	=> 500,
			'oauth_message' 	=> null,
			'message'       	=> 'Could not find authenticate a quickbooks connection.'
		];

		//TRY TO LOAD THE QUICKBOOKS DATASERVICE
		if($dataService = $this->_findQuickbooksDataService()){

			//DEFAULT MESSAGE FOR AUTHENTICATED BUT NO QB MODEL CLASS FOUND
			$qbModelMessage       	= (object)[
				'code'          	=> 500,
				'oauth_message' 	=> null,
				'message'       	=> 'Could not find quickbooks object: '.$this->quickbooks_entity
			];

			//TRY TO LOAD THE QUICKBOOKS MODEL CLASS 
			if($qbModelClass = $this->_getQuickbooksModelObject()){

				//CHECK IF WE MIGHT WANT TO UPSERT
				if($type == 'add' && $qbModel = $this->_quickbooksFindByFields()) $type = 'update';
				
				//CREATE THE MODEL
				if($type == 'add'){
					$qbModelResource 	= \QuickBooksOnline\API\Facades\FacadeHelper::reflectArrayToObject($this->quickbooks_entity, $this->quickbooksGenerateData('add'));
					$qbModelResult      = $dataService->Add($qbModelResource);
				}
				
				//UPDATE THE MODEL
				elseif($type == 'update'){
					$qbModel            = $dataService->FindbyId($this->quickbooks_entity, $this->quickbooks_id);
					$newQbModel 		= \QuickBooksOnline\API\Facades\FacadeHelper::reflectArrayToObject($this->quickbooks_entity, array_merge(['Id' => $this->quickbooks_id, 'SyncToken' => $this->quickbooks_synctoken, 'sparse' => true], $this->quickbooksGenerateData('update')));
			        $qbModelResource 	= \QuickBooksOnline\API\Facades\FacadeHelper::cloneObj($qbModel);
			        \QuickBooksOnline\API\Facades\FacadeHelper::mergeObj($qbModelResource, $newQbModel);
					$qbModelResult      = $dataService->Update($qbModelResource);
				}

				//DELETE THE MODEL
				elseif($type == 'delete'){
					$qbModel            = $dataService->FindbyId($this->quickbooks_entity, $this->quickbooks_id);
					$qbModelResult      = $dataService->Delete($qbModel);
				}

				//READ THE MODEL
				elseif($type == 'read'){

					if(!isset($this->quickbooks_id)) $this->_quickbooksFindByFields();
					
					$qbModel        = $dataService->FindbyId($this->quickbooks_entity, $this->quickbooks_id);
					$qbModelResult 	= $qbModel;
				}

				//VOID THE MODEL
				elseif($type == 'void'){
					$qbModel            = $dataService->FindbyId($this->quickbooks_entity, $this->quickbooks_id);
					$qbModelResult      = $dataService->Void($qbModel);
				}

				//CATCH INCORRECT REQUEST TYPES
				else{
					throw new \Exception(self::class." cannot use the quickbooks request type: {$type}");
				}

				//CHECK FOR ERRORS
				$error                  = $dataService->getLastError();

				if($error){

					$qbModelMessage     = (object)[
						'code'          => $error->getHttpStatusCode(),
						'oauth_message' => $error->getOAuthHelperError(),
						'message'       => $error->getResponseBody()
					];
				}
				else{

					//SUCCESSFULL TRANSACTION
					$success            = true;

					//SET THE MESSAGE
					$qbModelMessage     = (object)[
						'code'          => 200,
						'oauth_message' => null,
						'message'       => ucwords($type).' request for '.$this->quickbooks_entity.' was successfull.'
					];
				}
			}
		}

		$res = (object)[
			'success' 	=> $success,
			'type' 		=> $type,
			'message' 	=> $qbModelMessage,
			'result' 	=> $qbModelResult,
			'model' 	=> $this,
		];

		if($success){
			$this->quickbooks_id 			= $res->result->Id;
			$this->quickbooks_synctoken 	= $res->result->SyncToken;

			//SET REALM VALUE FOR SASS IF NEEDED
			if(is_null(config('quickbooks.sass')) || config('quickbooks.sass') === true) if(in_array('quickbooks_realm', $this->getFillable())) if($token = $this->_findQuickbooksAgencyToken()) $this->quickbooks_realm = $token->realm_id;
			$this->saveQuietly();
		}
		
		
		if($success && method_exists($this, 'quickbooksOnSuccess')){
			$this->quickbooksOnSuccess($res);
		}
		elseif(!$success && method_exists($this, 'quickbooksOnError')){
			$this->quickbooksOnError($res);
		}

		if(is_callable($callback)) $callback($res);

		return $res;
	}

	//VERIFY IF THE INHERITING MODEL HAS THE CORRECT FIELDS AND ATTRIBUTES
	private function _verifyQuickbooksAttributes(){

		if(!isset($this->quickbooks_entity)){
			throw new \Exception(self::class.' does not have a quickbooks_entity attribute defined in the model class.');
		}
		elseif(!in_array('quickbooks_id', $this->getFillable())){
			throw new \Exception(self::class.' does not have a fillable quickbooks_id in the model table.');
		}
		elseif(!in_array('quickbooks_synctoken', $this->getFillable())){
			throw new \Exception(self::class.' does not have a fillable quickbooks_synctoken in the model table.');
		}
	}

	//GET THE QUICKBOOKS SDK ENTITY CLASS
	private function _getQuickbooksModelObject(){

		//IF THE QUICKBOOKS ENTITY NAME IS NOT SET THEN DONT BOTHER
		if(!isset($this->quickbooks_entity)) return false;

		//BUILD THE CLASS NAME
		$className = '\QuickBooksOnline\API\Data\\IPP'.$this->quickbooks_entity;

		//SEND BACK THE CLASS IF IT EXISTS
		if(class_exists($className)) return new $className;

		//DEFAULT TO FALSE
		return false;
	}

	//GET THE AGENCY TOKEN
	private function _findQuickbooksAgencyToken(){

		//DONT BOTHER LOADING THE AGENCY TOKEN IF THE MODEL DOESNT EXIST
		if(!$this->exists) return null;

		//CHECK FOR SASS MODE
		if(is_null(config('quickbooks.sass')) || config('quickbooks.sass') === true){

			//TRY TO LOAD THE TOKEN
			try{
				return $this->quickBooksToken;
			}
			catch(\Exception $e){
				//SILENT
			}

			return null;
		}

		//FIND THE FIRST TOKEN IN THE DATABASE SINCE THIS IS NOT A SASS APP
		return \Itul\QuickBooks\Token::first();
	}

	//GET THE QUICKBOOKS DATA SERVICE FROM THE ASSOCIATED QUICKBOOKS AGENCY TOKEN
	private function _findQuickbooksDataService(){
		if($token = $this->_findQuickbooksAgencyToken()) return \Itul\QuickBooks\Helpers\QuickBooks::_setToken($token)->_dataService();
		return null;
	}
}