<?php

namespace Itul\QuickBooks;

use Exception;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Exception\SdkException;
use QuickBooksOnline\API\Exception\ServiceException;
use QuickBooksOnline\API\ReportService\ReportService;


class Client{

	protected $configs;
	protected $data_service;
	protected $report_service;
	protected $token;

	public function __construct(array $configs, Token $token){
		$this->configs = $configs;
		$this->setToken($token);
	}

	public function authorizationUri(){
		return $this->getDataService()->getOAuth2LoginHelper()->getAuthorizationCodeURL();
	}

	public function configureLogging(){
		// In case any of the keys are not in the configs, just disable logging
		try {
			if ($this->configs['logging']['enabled'] && dir($this->configs['logging']['location'])) {
				$this->data_service->setLogLocation($this->configs['logging']['location']);

				return $this->data_service->enableLog();
			}
		} catch (\Exception $e) {
			// TODO: Figure out what to do with this exception
		}

		return $this->data_service->disableLog();
	}

	public function deleteToken(){
		$this->setToken($this->token->remove());
		return $this;
	}

	public function exchangeCodeForToken($code, $realm_id){
		$oauth_token = $this->getDataService()->getOAuth2LoginHelper()->exchangeAuthorizationCodeForToken($code, $realm_id);
		$this->getDataService()->updateOAuth2Token($oauth_token);
		$this->token->parseOauthToken($oauth_token)->save();
		return $this;
	}

	public function getDataService(){
		if(!$this->hasValidAccessToken() || !isset($this->data_service)){
			$this->data_service = $this->makeDataService();
			$this->configureLogging();
		}

		return $this->data_service;
	}

	public function getReportService(){
		if(!$this->hasValidAccessToken() || !isset($this->report_service)) $this->report_service = new ReportService($this->getDataService()->getServiceContext());
		return $this->report_service;
	}

	public function hasValidAccessToken(){
		return $this->token->getHasValidAccessTokenAttribute();
	}

	public function hasValidRefreshToken(){
		return $this->token->getHasValidRefreshTokenAttribute();
	}

	public function forceRefreshToken(Token $token){
		if($this->token->id == $token->id){
			$data_service = $this->data_service;
	        $oauth_token = $data_service->getOAuth2LoginHelper()->refreshToken();
	        $data_service->updateOAuth2Token($oauth_token);
	        $this->token->parseOauthToken($oauth_token)->save();
	        $this->token->refresh();
	        return $this->token;
		}
		
		return $token;
	}

	protected function makeDataService(){

		// Associative array to use to filter out only the needed config keys when using existing token
		$existing_keys = [
			'auth_mode'    => null,
			'baseUrl'      => null,
			'ClientID'     => null,
			'ClientSecret' => null,
		];

		// Have good access & refresh, so allow app to run
		if ($this->hasValidAccessToken()) {
			// Pull in the configs from the token into needed keys from the configs
			return DataService::Configure(array_merge(array_intersect_key($this->parseDataConfigs(), $existing_keys), [
				'accessTokenKey'  => $this->token->access_token,
				'QBORealmID'      => $this->token->realm_id,
				'refreshTokenKey' => $this->token->refresh_token,
			]));
		}

		// Have refresh, so update access & allow app to run
		if ($this->hasValidRefreshToken()) {
			// Pull in the configs from the token into needed keys from the configs
			$data_service = DataService::Configure(array_merge(array_intersect_key($this->parseDataConfigs(), $existing_keys), [
				'QBORealmID'      => $this->token->realm_id,
				'refreshTokenKey' => $this->token->refresh_token,
			]));

			$oauth_token = $data_service->getOAuth2LoginHelper()->refreshToken();
			$data_service->updateOAuth2Token($oauth_token);
			$this->token->parseOauthToken($oauth_token)->save();

			return $data_service;
		}

		// Create new...
		return DataService::Configure($this->parseDataConfigs());
	}

	/**
	 * QuickBooks is not consistent on their naming of variables, so map them
	 */
	protected function parseDataConfigs(){
		return [
			'auth_mode'    => $this->configs['data_service']['auth_mode'],
			'baseUrl'      => $this->configs['data_service']['base_url'],
			'ClientID'     => $this->configs['data_service']['client_id'],
			'ClientSecret' => $this->configs['data_service']['client_secret'],
			'RedirectURI'  => route('quickbooks.token'),
			'scope'        => $this->configs['data_service']['scope'],
		];
	}
	
	public function setToken(Token $token){
		$this->token = $token;

		// The \QuickBooksOnline\API\DataService is tied to a specific token, so remove it when using a new one
		unset($this->data_service);
		
		//GET THE NEW DATA SERVICE
		$this->getDataService();

		return $this;
	}
}
